from django.conf import settings
from django.db import models
from django.contrib.postgres.fields import ArrayField
# Create your models here.
from django.utils import timezone

from blog.models import Post
import os


from app.settings import MEDIA_ROOT

profile_pics_path = os.path.join(MEDIA_ROOT, 'profile_pics')


class Profile(models.Model):

    user = models.OneToOneField(
        to=settings.AUTH_USER_MODEL, related_name='profile', on_delete=models.CASCADE,
    )

    followees = models.ManyToManyField( # one profilee can follow +1 users, and one users can follow many profiles
        to=settings.AUTH_USER_MODEL,
        related_name='followers',
        blank=True,
    )

    about = models.TextField(max_length=350, blank=True, null=True)

    liked_things = ArrayField(base_field=models.TextField(max_length=20),
                              verbose_name='Things I like', null=True, blank=True)
    image = models.ImageField(default='default.png', upload_to='media_cdn/profile_pics')
    location = models.TextField(max_length=100, blank=True, null=True)
    phone = models.TextField(max_length=17, blank=True, null=True)

    def __str__(self):
        return self.user.username + 's Profile'


class Like(models.Model):

    # Todo: user and post together unique
    post = models.ForeignKey(to='blog.Post', related_name='likes', on_delete=models.CASCADE)
    user = models.ForeignKey(to=settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name='likes')
    created = models.DateField(auto_now_add=True, verbose_name='Like created')

    def __str__(self):
        return self.user.username + ' likes ' + self.post.title
