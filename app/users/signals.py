from django.contrib.auth import get_user_model
from django.db.models.signals import post_save
from django.dispatch import receiver

from users.models import Profile


@receiver(post_save, sender=get_user_model())
def create_profile(**kwargs):
    instance = kwargs.get('instance')
    if kwargs.get('created'):
        Profile.objects.create(user=instance)