import pytest
from django.contrib.auth import get_user_model
from mixer.backend.django import mixer
from rest_framework.reverse import reverse

pytestmark = pytest.mark.django_db

User = get_user_model()


class TestFollowersList:

    # url = '/api/users/followers/'
    url = reverse('users:followers-list')

    def test_successful(self, user1_client, user1, user2):
        user2.profile.followees.add(user1)
        resp = user1_client.get(self.url)
        assert resp.status_code == 200
        assert len(resp.data) == 1

    def test_anonymous(self, api_client):
        resp = api_client.get(self.url)
        assert resp.status_code == 403, 'View locked for anon'


class TestFolloweesList:

    url = reverse('users:followees-list')

    def test_successful(self, user1_client, user1, user2):
        user1.profile.followees.add(user2)
        resp = user1_client.get(self.url)
        assert resp.status_code == 200
        assert len(resp.data) == 1

    def test_anonymous(self, api_client):
        resp = api_client.get(self.url)
        assert resp.status_code == 403, 'View locked for anon'


class TestFollowUser:

    @staticmethod
    def get_url(user_id):
        return reverse('users:follow-user', kwargs={'user_id': user_id})

    def test_follow_unfollow(self, user1, user2, user1_client):
        url = self.get_url(user2.id)
        resp = user1_client.post(url)
        assert resp.status_code == 201
        assert len(user1.profile.followees.all()) == 1

        # unfollow user
        resp = user1_client.delete(url)
        assert resp.status_code == 204
        assert len(user1.profile.followees.all()) == 0

    def test_anonymous(self, api_client):
        resp = api_client.get(self.get_url(1000))
        assert resp.status_code == 403, 'View locked for anon'


class TestUserListView:

    url = reverse('users:users-list')

    def test_get_all_users(self, user1_client):
        resp = user1_client.get(self.url)
        assert resp.status_code == 200
        assert len(resp.data) == 3, 'should return the number of users mocked initially'

    def test_anonymous(self, api_client):
        resp = api_client.get(self.url)
        assert resp.status_code == 403, 'View locked for anon'


class TestUserDetailView:

    @staticmethod
    def get_url(user_id):
        return reverse('users:users-detail', kwargs={'user_id': user_id})

    def test_get_user_profile(self, user1_client, user2):
        resp = user1_client.get(self.get_url(user2.id))
        assert resp.status_code == 200
        print(len(resp.data))
        assert len(resp.data) == 6, 'Initially there should be 6 fields'

    def test_anonymous(self, api_client):
        resp = api_client.get(self.get_url(1000))
        assert resp.status_code == 403, 'View locked for anon'


class TestGetUpdateOwnProfile:

    url = reverse('users:own-profile-get-update')

    def test_get_profile_information(self, user1_client):
        resp = user1_client.get(self.url)
        assert resp.status_code == 200
        user_fields = ['id', 'username', 'first_name', 'last_name', 'email']
        for field in user_fields:
            assert resp.data[field], f'{field} should be given back as part of the users profile'

        profile_fields = ['id', 'image', 'location', 'phone', 'liked_things']
        for field in profile_fields:
            try:
                resp.data['profile'][field]
            except:
                assert False, f'{field} should be given back as part of the users profile'

    def test_update_profile(self, user1_client):

        test = ['first_name', 'last_name', 'email', 'username',
                  'password', 'password2',
                  'followees', 'about', 'liked_things', 'image', 'location', 'phone']

        data = dict(
            first_name = '1',
            last_name = '2',
            username='john',
            profile=dict(
                about='dfasdflköjfdölkjdsfasdfkjdsölif ds fölkjs ölk dsfölksdfaölkjdsa fölkj dsafölkj dsölkds ',
                location='earth',
                nonsense='test',
                phone='4',
                liked_things = ['eating', 'sleeping']
            )
        )
        resp = user1_client.patch(self.url, data=data, format='json')
        assert resp.status_code == 200
        assert resp.data['username'] == 'john'
        assert resp.data['profile']['location'] == 'earth'
        assert resp.data['profile']['phone'] == '4'
        assert resp.data['profile']['liked_things'] == ['eating', 'sleeping']

    def test_update_username_existing(self, user1_client):
        resp = user1_client.patch(self.url, data={'username': 'admin'})
        assert resp.status_code == 400
        assert resp.data['username'] != 'admin'

    def test_unique_email(self, user1_client):
        email = 'test@test.ch'
        mixer.blend(User, email=email)
        resp = user1_client.patch(self.url, data={'email': email})
        assert resp.status_code == 400
        assert resp.data['email']

    def test_anonymous(self, api_client):
        resp = api_client.get(self.url)
        assert resp.status_code == 403, 'View locked for anon'