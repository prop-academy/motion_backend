from django.contrib.auth import get_user_model

# Create your views here.
from rest_framework import status
from rest_framework.generics import ListAPIView, GenericAPIView, RetrieveAPIView, RetrieveUpdateAPIView
from rest_framework.parsers import JSONParser, MultiPartParser
from rest_framework.response import Response

from users.serializers import UserSerializer, ProfileSerializer

User = get_user_model()

class UserFollowersListApiView(ListAPIView):
    """
    List of all the logged in user’s followers
    """
    serializer_class = UserSerializer

    def get_queryset(self):
        return [up.user for up in self.request.user.followers.all()]


class UserFollowingListApiView(ListAPIView):
    """
    List of all the logged in user’s followers
    """
    serializer_class = UserSerializer

    def get_queryset(self):
        return self.request.user.profile.followees.all()


class FollowDetailApiView(GenericAPIView):
    """
    follow user with id (POST), unfollow user with id (DELETE).
    """

    def post(self, request, user_id):
        id = user_id
        # check if you follow him already
        following = request.user.profile.followees.filter(id=id)
        if following.exists():
            return Response('Already Following this user', status=404)
        # Todo: is there something like add_or_get?
        user_to_follow = User.objects.get(id=id)
        request.user.profile.followees.add(user_to_follow)
        return Response(status=status.HTTP_201_CREATED, data={
            'detail': 'Successfully following user'
        })

    def delete(self, request, user_id):
        receiver_id = user_id
        try:
            followed_user = request.user.profile.followees.get(id=receiver_id)
            # Todo, check status code and if possible use a built-in response
            request.user.profile.followees.remove(followed_user)
            return Response(status=status.HTTP_204_NO_CONTENT)
        except User.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)


class UsersListApiView(ListAPIView):
    """
    Get the list of all the users
    """
    serializer_class = UserSerializer

    def get_queryset(self):
        query = self.request.query_params.get('search', None)
        if query:
            all_users = User.objects.filter(
                username__contains=query) | \
                        User.objects.filter(
                            first_name__contains=query) | \
                        User.objects.filter(
                            last_name__contains=query)
            return all_users

        return User.objects.all()


class UserProfileDetailApiView(RetrieveAPIView):
    """
    Get specific user profile
    """
    serializer_class = UserSerializer
    lookup_url_kwarg = 'user_id'
    queryset = User.objects.all()

    # def get_object(self):
    #     obj = super().get_object()
    #     return obj.profile


class OnwProfileView(RetrieveUpdateAPIView):
    """
    Get profile of logged in user
    """

    queryset = User.objects.all()
    serializer_class = UserSerializer
    parser_classes = (
        JSONParser,
        MultiPartParser
    )

    # def get_serializer_class(self):
    #     method = self.request.method
    #     if method == 'PUT' or method == 'PATCH':
    #         return UserProfileUpdateSerializer
    #     else: return GetProfileSerializer

    def get_object(self):
        self.kwargs['pk'] = self.request.user.pk
        return super().get_object()
        # obj = super().get_object()
        # return obj.profile
