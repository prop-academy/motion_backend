from django.contrib.auth import get_user_model
from django.utils import timezone
from rest_framework.serializers import ModelSerializer

from authorisation.models import UserToken
from users.models import Profile
from rest_framework import serializers
from django.contrib.auth.password_validation import validate_password as django_validate_password
from django.core.exceptions import ValidationError as DjangoValidationError

User = get_user_model()

class LikeSerializer(ModelSerializer):

    class Meta:
        fields = ('post', 'user', 'created')


class ProfileSerializer(ModelSerializer):
    class Meta:
        model = Profile
        fields = ('id', 'image', 'location', 'phone', 'liked_things')

        extra_kwargs = {
            'id': {'read_only': True},
            'image': {'read_only': True}
        }

    # def update(self, instance, validated_data):
    #     user_data = validated_data.pop('user')
    #     # Unless the application properly enforces that this field is
    #     # always set, the follow could raise a `DoesNotExist`, which
    #     # would need to be handled.
    #     user = instance.user
    #
    #     instance.username = validated_data.get('username', instance.username)
    #     instance.email = validated_data.get('email', instance.email)
    #     instance.save()
    #
    #     profile.is_premium_member = profile_data.get(
    #         'is_premium_member',
    #         profile.is_premium_member
    #     )
    #     profile.has_support_contract = profile_data.get(
    #         'has_support_contract',
    #         profile.has_support_contract
    #     )
    #     profile.save()
    #
    #     return instance


class UserSerializer(ModelSerializer):
    # validate_username is not required, comes from model user throws validation error
    profile = ProfileSerializer(required=False)

    class Meta:
        model = User
        fields = ('id', 'username', 'first_name', 'last_name', 'email', 'password', 'profile')
        depth = 1

        extra_kwargs = {
            'id': {'read_only': True},
            'image' : {'read_ony': True},
            'password': {'write_only': True},
        }

    def validate_email(self, email):
        try:
            User.objects.get(email=email)
            raise serializers.ValidationError('Email is already taken')
        except User.DoesNotExist:
            return email

    def update(self, instance, validated_data):
        if 'profile' in validated_data.keys():
            profile_data = validated_data.pop('profile')
            for attr, value in profile_data.items():
                setattr(instance.profile, attr, value)
            instance.profile.save()
        user = super().update(instance, validated_data)
        return user

    def create(self, validated_data):
        profile_data = validated_data.pop('profile')
        user = User(**validated_data)
        user.set_password(validated_data['password'])
        user.save()

        for attr, value in profile_data.items():
            setattr(user.profile, attr, value)
        user.profile.save()
        return user





class PasswordResetValidationSerializer(serializers.Serializer):

    password = serializers.CharField(max_length=100)
    password2 = serializers.CharField(max_length=100)
    token_type = 'password-reset'

    class Meta:
        fields = ('email', 'token', 'type', 'password', 'password2')

    def validate(self, attrs):

        if attrs['password'] != attrs['password2']:
            return serializers.ValidationError({
                'password': 'The provided passwords must be equal'})
        try:
            usr_token_obj = UserToken.objects.get(token=attrs['token'], email=attrs['email'], type=self.token_type)
        except UserToken.DoesNotExist:
            raise serializers.ValidationError("Wrong email or validation code")

        return attrs

    def validate_type(self, type):
        if not type == self.token_type:
            raise serializers.ValidationError('Wrong token type from frontend.')
        return type

    def validate_token(self, token):
        try:
            token_obj = UserToken.objects.get(token=token)
        except UserToken.DoesNotExist:
            raise serializers.ValidationError("Wrong validation code")

        if token_obj.expires < timezone.now():
            raise serializers.ValidationError("Token has expired")
        return token

    def validate_password(self, value):
        try:
            django_validate_password(password=value)
            return value
        except DjangoValidationError as e:
            raise serializers.ValidationError(e.message)

    def validate_email(self, email):
        try:
            User.objects.get(email=email)
        except User.DoesNotExist:
            if self.token_type == 'password-reset':
                raise serializers.ValidationError("Can not reset password for non existing user")
        return email


class RegistrationSerializer(serializers.Serializer):

    email = serializers.EmailField(
        label='Registration Email Address'
    )
    type = 'registration'

    class Meta:
        fields = ('email',)

    def validate_email(self, value):
        try:
            User.objects.get(email=value)
            raise serializers.ValidationError('User already exists')
        except User.DoesNotExist:
            return value


class RegistrationValidationSerializer(PasswordResetValidationSerializer):

    token_type = 'registration'

    class Meta:
        fields = ('email', 'first_name', 'last_name', 'type', 'token', 'password', 'password2')

    def validate_email(self, email):
        try:
            User.objects.get(email=email)
        except User.DoesNotExist:
            return email
        raise serializers.ValidationError("Email is already taken.")


# class UserProfileUpdateSerializer(serializers.Serializer):
#     first_name = serializers.CharField(required=False)
#     last_name = serializers.CharField(required=False)
#     email = serializers.EmailField(required=False)
#     username = serializers.CharField(required=False)
#     password = serializers.CharField(required=False)
#     password2 = serializers.CharField(required=False)
#     about = serializers.CharField()
#     # liked_things = serializers.
#
#     class Meta:
#         model = Profile
#         fields = ('first_name', 'last_name', 'email', 'username',
#                   'password', 'password2',
#                   'about', 'liked_things', 'image', 'location', 'phone')
#
#     def validate(self, attrs):
#         if attrs['password'] != attrs['password2']:
#             return serializers.ValidationError({
#                 'password': 'The provided passwords must be equal'})
#
#     def validate_email(self, email):
#         try:
#             User.objects.get(email=email)
#             raise serializers.ValidationError('Email is already taken')
#         except User.DoesNotExist:
#             return email
#
#     def validate_username(self, name):
#         try:
#             User.objects.get(usename=name)
#             raise serializers.ValidationError('Username already exists')
#         except User.DoesNotExist:
#             return name
#
#     def validate_password(self, pw):
#         try:
#             django_validate_password(password=pw)
#             return pw
#         except DjangoValidationError as e:
#             raise serializers.ValidationError(e.message)
#
#     def update(self, instance, validate_data):
#         # instance must be profile
#         instance.first_name =
#
#         # user_ser = UserSerializer(data=data)
#         # user_ser.is_valid()
#         # user_data = user_ser.validated_data




