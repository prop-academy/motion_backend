from django.urls import path

from users.views import UserFollowersListApiView, UserFollowingListApiView, FollowDetailApiView, UsersListApiView, \
    UserProfileDetailApiView, OnwProfileView

app_name = 'users'

urlpatterns = [
    path('users/followers/', UserFollowersListApiView.as_view(), name='followers-list'),
    path('users/following/', UserFollowingListApiView.as_view(), name='followees-list'),
    path('users/follow/<int:user_id>/', FollowDetailApiView.as_view(), name='follow-user'),
    path('users/', UsersListApiView.as_view(), name='users-list'),
    path('users/<int:user_id>/', UserProfileDetailApiView.as_view(), name='users-detail'),
    path('me/', OnwProfileView.as_view(), name='own-profile-get-update')

]