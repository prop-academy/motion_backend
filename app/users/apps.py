from django.apps import AppConfig


class UsersConfig(AppConfig):
    name = 'users'

    # import the signals
    def ready(self):
        import users.signals    # noqa