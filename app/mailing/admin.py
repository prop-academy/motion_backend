from django.contrib import admin

# Register your models here.
from mailing.models import Email

admin.site.register(Email)