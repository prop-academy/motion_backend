from django.contrib.postgres.fields import ArrayField
from django.db import models
from django.utils import timezone


class Email(models.Model):

    subject = models.CharField(max_length=100)
    body = models.TextField(max_length=1000)
    from_email = models.EmailField()
    to = ArrayField(base_field=models.EmailField(unique=True))
    cc = ArrayField(base_field=models.EmailField(unique=True), null=True, blank=True)
    bcc = ArrayField(base_field=models.EmailField(unique=True), null=True, blank=True)
    reply_to = models.EmailField(blank=True, null=True)
    created = models.DateTimeField(default=timezone.now)
    sent = models.BooleanField(default=False)
