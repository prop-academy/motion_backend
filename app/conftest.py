import pytest
from django.conf import settings
from django.contrib.auth import get_user_model
from django.urls import reverse_lazy
from mixer.backend.django import mixer
import os

from rest_framework.test import APIClient

from blog.models import Post

admin_pw = 'admin_123'
user1_pw = 'user1_123'
user2_pw = 'user2_123'

MOCK_POSTS_NUM = 5


def create_some_posts(how_many=10, owner_ids=None):
    if not owner_ids:
        owner_ids = [1,]
    for id in owner_ids:
        for i in range(how_many):
            mixer.blend(Post, owner_id=id)


def fill_database():
    user1 = mixer.blend(settings.AUTH_USER_MODEL,
                        username='user1',
                        email='user1@test.ch')
    user2 = mixer.blend(settings.AUTH_USER_MODEL,
                        username='user2',
                        email='user2@test.ch')
    admin = mixer.blend(
        settings.AUTH_USER_MODEL,
        username='admin',
        is_superuser=True,
        email='admin@test.ch'
    )

    user1.set_password(user1_pw)
    user2.set_password(user2_pw)
    admin.set_password(admin_pw)

    user1.save()
    user2.save()
    admin.save()
    owner_ids = [user1.pk, user2.pk, admin.pk]
    create_some_posts(how_many=MOCK_POSTS_NUM, owner_ids=owner_ids)


@pytest.fixture(scope='session', autouse=True)
def django_db_setup(django_db_setup, django_db_blocker):
    from django.conf import settings
    settings.DATABASES['default'] = {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'test',
        'USER': os.environ.get('POSTGRES_USER'),
        'PASSWORD': os.environ.get('POSTGRES_PASSWORD'),
        'HOST': 'postgres',
        'PORT': '5432',
    }
    with django_db_blocker.unblock():
        fill_database()


pytestmark = pytest.mark.django_db


@pytest.fixture(scope='function')
def get_jwt_tokens():
    def get_token(username, password):
        client = APIClient()
        # access_obtain_url = reverse_lazy('auth')
        access_obtain_url = reverse_lazy('authorisation:token_obtain_pair')
        data = {'username': username, 'password': password}
        resp = client.post(access_obtain_url, data=data)
        return resp

    return get_token


@pytest.fixture(scope='function')
def get_auth_client(get_jwt_tokens):
    def get_client(username, password):
        client = APIClient()
        resp = get_jwt_tokens(username=username, password=password)
        acc_token = resp.data['access']
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + acc_token)
        return client

    return get_client


@pytest.fixture(scope='function')
def admin_client(get_auth_client):
    return get_auth_client(username='admin', password=admin_pw)


@pytest.fixture(scope='function')
def user1_client(get_auth_client):
    return get_auth_client(username='user1', password=user1_pw)


@pytest.fixture(scope='function')
def user2_client(get_auth_client):
    return get_auth_client(username='user2', password=user2_pw)


# @pytest.fixture(scope='function')
# def api_rf():
#     factory = APIRequestFactory()
#     return factory


@pytest.fixture(scope='function')
def api_client():
    client = APIClient()
    return client

@pytest.fixture(scope='function')
def user1():
    return get_user_model().objects.get(username='user1')


@pytest.fixture(scope='function')
def user2():
    return get_user_model().objects.get(username='user2')


@pytest.fixture(scope='function')
def admin():
    return get_user_model().objects.get(username='admin')


@pytest.fixture(scope='function')
def user1_post(user1):
    return Post.objects.filter(owner=user1).first()

@pytest.fixture(scope='function')
def user2_post(user2):
    return Post.objects.filter(owner=user2).first()