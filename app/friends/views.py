from django.contrib.auth import get_user_model
from django.shortcuts import render

# Create your views here.
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.generics import ListAPIView, get_object_or_404, GenericAPIView, DestroyAPIView
from rest_framework.response import Response

from friends.models import FriendRequest
from friends.serializers import FriendRequestSerializer
from users.serializers import UserSerializer


class OpenFriendRequestsApiView(ListAPIView):
    """
    List all open friend requests from other users
    """
    serializer_class = FriendRequestSerializer

    def get_queryset(self):
        pending = FriendRequest.objects.filter(
            receiver=self.request.user,
            status=FriendRequest.PENDING)
        return pending


class PendingFriedRequestsApiView(ListAPIView):
    """
    List all pending friend requests sent to other users
    """
    serializer_class = FriendRequestSerializer

    def get_queryset(self):
        pending = FriendRequest.objects.filter(
            sender=self.request.user,
            status='p')
        return pending


@api_view(['POST'])
def accept_friend_request(request, request_id):
    """
    Accept friend request
    """
    to_update = get_object_or_404(FriendRequest, id=request_id)
    if to_update.receiver != request.user:
        return Response({'detail': 'Can only accept friend requests sent from others'},
            status=status.HTTP_400_BAD_REQUEST)
    if to_update.status != FriendRequest.PENDING:
        return Response({'detail':'Friendrequest can not be changed since status was not pending'},
                        status=status.HTTP_400_BAD_REQUEST)
    to_update.status = FriendRequest.ACCEPTED
    to_update.save()
    seri = FriendRequestSerializer(instance=to_update)
    return Response(status=status.HTTP_201_CREATED, data=seri.data)


@api_view(['POST'])
def reject_friend_request(request, request_id):
    """
    Accept friend request
    """
    to_update = get_object_or_404(FriendRequest, id=request_id)
    if to_update.status != FriendRequest.PENDING:
        return Response({'detail':'Friendrequest can not be changed since status was not pending'},
                        status=status.HTTP_400_BAD_REQUEST)
    if to_update.receiver != request.user:
        return Response(status=status.HTTP_403_FORBIDDEN)
    to_update.status = FriendRequest.DECLINED
    to_update.save()
    seri = FriendRequestSerializer(instance=to_update)
    return Response(status=status.HTTP_201_CREATED, data=seri.data)


class FriendsListApiView(ListAPIView):
    serializer_class = UserSerializer

    def get_queryset(self):
        user = self.request.user
        friends = get_user_model().objects.filter(
            received_requests__sender=user,
            received_requests__status=FriendRequest.ACCEPTED) | \
                  get_user_model().objects.filter(
                      sent_requests__receiver=user,
                      sent_requests__status=FriendRequest.ACCEPTED)
        return friends


class CreateFriendRequestView(GenericAPIView):
    lookup_url_kwarg = 'user_id'
    queryset = get_user_model().objects.all()

    def post(self, request, user_id):
        receiver = self.get_object()
        if receiver == request.user:
            return Response(status=status.HTTP_400_BAD_REQUEST, data={
                'detail': 'Can not make friends with yourself.'
            })

        try:
            FriendRequest.objects.get(sender=receiver, receiver=request.user)
            return Response({'detail': 'Friend request was already sent by other user'},
                            status=status.HTTP_400_BAD_REQUEST)

        except FriendRequest.DoesNotExist:
            pass

        new_req, created = FriendRequest.objects.get_or_create(
            sender=self.request.user,
            receiver=receiver)
        if created:
            new_req.save()
            return Response(status=status.HTTP_201_CREATED, data={
                'detail': f'Friendrequest to user {receiver.username} sent'
            })
        else:
            return Response(status=status.HTTP_400_BAD_REQUEST, data={'detail': 'Friend request was already sent'})


class UnfriendApiView(DestroyAPIView):
    """
    Unfriend a friend, so to delete the entry in the FriendRequestTable.
    Like this, the user can send you a friend request again.
    """
    lookup_url_kwarg = 'user_id'
    serializer_class = FriendRequestSerializer
    # queryset = FriendRequest.objects.all()

    def get_object(self):
        other_user_id = self.kwargs['user_id']
        record_set = FriendRequest.objects.filter(sender=self.request.user, receiver_id=other_user_id) | \
                     FriendRequest.objects.filter(sender_id=other_user_id, receiver=self.request.user)
        return record_set.first()
