from rest_framework.serializers import ModelSerializer

from friends.models import FriendRequest


class FriendRequestSerializer(ModelSerializer):

    class Meta:
        model = FriendRequest
        fields = ('sender', 'receiver', 'status', 'created')
