from django.urls import path

from friends.views import CreateFriendRequestView, OpenFriendRequestsApiView, PendingFriedRequestsApiView, \
    accept_friend_request, reject_friend_request, FriendsListApiView, UnfriendApiView

app_name = 'friends'


urlpatterns = [
    path('users/friendrequests/<int:user_id>/', CreateFriendRequestView.as_view(), name='friendrequests-create'),
    path('users/friendrequests/', OpenFriendRequestsApiView.as_view(), name='open-friendrequests-list'),
    path('users/friendrequests/pending/', PendingFriedRequestsApiView.as_view(), name='pending-friendrequests-list'),
    path('users/friendrequests/accept/<int:request_id>/', accept_friend_request, name='accept-friendrequest'),
    path('users/friendrequests/reject/<int:request_id>/', reject_friend_request, name='decline-friendrequest'),
    path('users/friends/', FriendsListApiView.as_view(), name='friends-list'),
    path('users/friends/unfriend/<int:user_id>/', UnfriendApiView.as_view(), name='unfriend-delete'),
]

