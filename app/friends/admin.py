from django.contrib import admin

# Register your models here.
from friends.models import FriendRequest

admin.site.register(FriendRequest)