from django.conf import settings
from django.db import models

# Create your models here.
class FriendRequest(models.Model):

    ACCEPTED = 'a'
    DECLINED = 'd'
    PENDING = 'p'

    STATUS = (
        (PENDING, 'pending'),
        (ACCEPTED, 'accepted'),
        (DECLINED, 'declined')
    )

    sender = models.ForeignKey(
        to=settings.AUTH_USER_MODEL, related_name='sent_requests',
        on_delete=models.CASCADE,
        verbose_name='Friend Request Sender',
        null=False
        )

    receiver = models.ForeignKey(
        to=settings.AUTH_USER_MODEL,
        related_name='received_requests',
        on_delete=models.CASCADE,
        verbose_name='Friend Request Receiver',
        null=False
    )

    class Meta:
        unique_together = ('sender', 'receiver',)

    status = models.TextField(max_length=3, choices=STATUS, default='p')
    created = models.DateField(auto_now_add=True, verbose_name='Friend Request created')

    def __str__(self):
        return self.sender.username + " to " + self.receiver.username