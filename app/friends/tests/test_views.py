import pytest
from django.conf import settings
from django.contrib.auth import get_user_model
from django.core.files.uploadedfile import SimpleUploadedFile
from django.urls import reverse_lazy
from mixer.backend.django import mixer
from rest_framework import status

from blog.models import Post
from conftest import MOCK_POSTS_NUM
from friends.models import FriendRequest
from users.models import Profile, Like
import os
pytestmark = pytest.mark.django_db


class TestCreateFriendRequest:

    @staticmethod
    def get_url(user_id):
        return reverse_lazy('friends:friendrequests-create', kwargs={'user_id': user_id})

    def test_successful(self, user1_client, user2, user1):

        resp = user1_client.post(self.get_url(user2.id))
        assert resp.status_code == 201

        new_req = FriendRequest.objects.filter(sender=user1, receiver=user2)
        assert new_req.exists(), 'The queryset should return a new entry created in the database'
        assert len(new_req) == 1, 'Should return exactly one entry'

    def test_anonymous(self, api_client):
        resp = api_client.get(self.get_url(1000))
        assert resp.status_code == 403, 'View locked for anon'


class TestPendingFriendRequests:

    url = reverse_lazy('friends:open-friendrequests-list')

    def test_successful(self, user1_client, user2, user1):
        # also tests of the default of the creation will be pending
        mixer.blend(FriendRequest, sender=user1, receiver=user2)

        resp = user1_client.get(self.url)
        assert resp.status_code == 200
        assert len(resp.data) == 0, 'Show only pending sent from other users'

        mixer.blend(FriendRequest, sender=user2, receiver=user1)
        resp = user1_client.get(self.url)
        assert resp.status_code == 200
        assert len(resp.data) == 1, 'Should return one pending friend request from user2'

    def test_anonymous(self, api_client):
        resp = api_client.get(self.url)
        assert resp.status_code == 403, 'View locked for anon'


class TestAcceptRejectFriendrequest:

    @staticmethod
    def get_url(req_id, action):
        return reverse_lazy(f'friends:{action}-friendrequest', kwargs={'request_id': req_id})

    def test_accept_reject_own_invitation(self, user1_client, user1, user2):
        bad_req = mixer.blend(FriendRequest, sender=user1, receiver=user2)

        # try: user1 want to accept his own sent friendrequest
        resp = user1_client.post(self.get_url(bad_req.id, 'accept'))
        assert resp.status_code == 400, 'Should not be able to accept a friend request sent by user itself'

    def test_friend_and_unfriend(self, user1_client, user1, user2):
        # try: user1 wants to accept request his is reveiver of
        new_req = mixer.blend(FriendRequest, sender=user2, receiver=user1)
        resp = user1_client.post(self.get_url(new_req.id, 'accept'))
        assert resp.status_code == 201, 'user1 should be able to accept friend request of sender user2'

        new_req.refresh_from_db()
        assert new_req.status == FriendRequest.ACCEPTED

        resp = user1_client.post(self.get_url(new_req.id, 'decline'))
        assert resp.status_code == 400, 'Once friendrequest is accepted, then delete but not change to decline again'

    def test_anonymous(self, api_client):
        resp = api_client.get(self.get_url(1000, 'accept'))
        assert resp.status_code == 403, 'View locked for anon'

        resp = api_client.get(self.get_url(1000, 'decline'))
        assert resp.status_code == 403, 'View locked for anon'


class TestFriendsList:

    url = reverse_lazy('friends:friends-list')

    def test_successful(self, user1_client, user1, user2, admin):
        mixer.blend(FriendRequest, sender=user1, receiver=user2, status=FriendRequest.ACCEPTED)
        mixer.blend(FriendRequest, sender=admin, receiver=user1, status=FriendRequest.ACCEPTED)

        resp = user1_client.get(self.url)
        assert resp.status_code == 200
        assert len(resp.data) == 2, 'user1 should now have two friends admin and user2'

    def test_anonymous(self, api_client):
        resp = api_client.get(self.url)
        assert resp.status_code == 403, 'View locked for anon'


class TestUnfriendView:

    @staticmethod
    def get_url(user_id):
        return reverse_lazy('friends:unfriend-delete', kwargs={'user_id': user_id})

    def test_successful(self, user2_client, user1, admin, user2):
        mixer.blend(FriendRequest, sender=user1, receiver=user2, status=FriendRequest.ACCEPTED)
        mixer.blend(FriendRequest, sender=user2, receiver=admin, status=FriendRequest.ACCEPTED)

        # Test as the user2 was reveiver of the friend request
        resp = user2_client.delete(self.get_url(user1.id))
        assert resp.status_code == 204, 'Return no content when deleted successfully'
        assert not FriendRequest.objects.filter(sender=user1, receiver=user2).exists()

        # Test as the user2 was reveiver of the friend request
        resp = user2_client.delete(self.get_url(admin.id))
        assert resp.status_code == 204, 'Return no content when deleted successfully'
        assert not FriendRequest.objects.filter(sender=user2, receiver=admin).exists()

    def test_anonymous(self, api_client):
        resp = api_client.get(self.get_url(1))
        assert resp.status_code == 403, 'View locked for anon'

