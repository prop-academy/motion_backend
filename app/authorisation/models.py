import datetime as dt

from django.db import models

# Create your models here.
from django.utils import timezone

from app import settings
from authorisation.helpers import generate_simple_token


def in_a_day():
    return timezone.now() + timezone.timedelta(days=settings.SIMPLE_TOKEN_EXPIRATION_DAYS)

class UserToken(models.Model):

    TYPE_CHOICES = (
        ('registration', 'registration'),
        ('password-reset', 'password-reset')
    )

    expires = models.DateTimeField(default=in_a_day, blank=True)
    email = models.EmailField()
    token = models.CharField(max_length=6, null=True, blank=True, default=generate_simple_token)
    type = models.CharField(choices=TYPE_CHOICES, max_length=14, blank=True)

    def generate_token(self):
        self.token = generate_simple_token()
        self.save()
        return self.token

    def __str__(self):
        return self.email + ", type: " + self.type