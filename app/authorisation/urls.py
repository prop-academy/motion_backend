from django.urls import path
from rest_framework_simplejwt.views import TokenObtainPairView, TokenRefreshView

from authorisation.views import PasswordResetView, ValidateResetPasswordView, SendRegistrationTokenView, \
    RegistrationValidationView

app_name = 'authorisation'

urlpatterns = [
    path('auth/token/', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('auth/token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
    path('auth/password-reset/', PasswordResetView.as_view(), name='password-reset'),
    path('auth/password-reset/validate/', ValidateResetPasswordView.as_view(), name='password-reset-validate'),
    path('registration/', SendRegistrationTokenView.as_view(), name='registration'),
    path('registration/validation/', RegistrationValidationView.as_view(), name='registration-validation')
]
