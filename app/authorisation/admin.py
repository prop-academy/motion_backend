from django.contrib import admin

# Register your models here.
from authorisation.models import UserToken

admin.site.register(UserToken)