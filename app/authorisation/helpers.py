import random

from django.utils import timezone


def code_generator(lenght=5):
    numbers = '0123456789'
    return "".join(random.choice(numbers) for i in range(lenght))


def generate_simple_token(length=6):
    return ''.join([str(random.randint(0,9)) for i in range(length-1)])


def in_a_day():
    return timezone.now() + timezone.timedelta(days=1)