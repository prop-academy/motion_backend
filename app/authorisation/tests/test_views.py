import pytest
from django.conf import settings
from django.contrib.auth import get_user_model
from django.urls import reverse_lazy
from rest_framework.test import APIClient
from mixer.backend.django import mixer

from authorisation.models import UserToken

pytestmark = pytest.mark.django_db


class TestObtainJWTTokens:

    def test_token_obtain_pair_admin(self, admin_client):
        # Check if a token is attached to the AUTHORISATION
        assert len(admin_client._credentials['HTTP_AUTHORIZATION']) > 100, 'Fixtures should auth with JWT'

    def test_token_obtain_pair_user1(self, user1_client):
        # Check if a token is attached to the AUTHORISATION
        assert len(user1_client._credentials['HTTP_AUTHORIZATION']) > 100, 'Fixtures should auth with JWT'

    def test_token_refresh(self, get_jwt_tokens, api_client):
        user = mixer.blend(settings.AUTH_USER_MODEL, username='tester')
        user.set_password('thisisfortesting_123')
        user.save()
        user.refresh_from_db()
        resp = get_jwt_tokens(username='tester', password='thisisfortesting_123')
        refresh_url = reverse_lazy('authorisation:token_refresh')
        new_resp = api_client.post(refresh_url, data={'refresh': resp.data['refresh']})
        assert new_resp.data['access'], 'There should be a new access token'


class TestPasswordReset:
    url = reverse_lazy('authorisation:password-reset')

    def test_wrong_email_format(self, api_client):
        resp = api_client.post(self.url, data={'email': 'tester@tester'})
        assert resp.status_code == 400, 'Should not accept wrong email'
        assert resp.data['email'], 'Failure should be due to the email'

    def test_get_token_for_pw_reset_anonymous(self, api_client):
        resp = api_client.post(self.url)
        assert resp.status_code == 400, 'Email is requiered to get a code'
        assert resp.data['email'], 'Failure should be due to the email'

        resp = api_client.post(self.url, data={'email': 'tester@tester.ch'})
        # Todo: test if email was sent in memory
        assert resp.status_code == 201, 'Should return created as for post requests if email was sent'


class TestPasswordResetValidation:
    url = reverse_lazy('authorisation:password-reset-validate')

    def test_successfull_registration(self, api_client):
        # todo: check the email box and get the code
        pass


class TestRegistration:
    url = reverse_lazy('authorisation:registration')
    url_validate = reverse_lazy('authorisation:registration-validation')

    def test_wrong_email_format(self, api_client):
        resp = api_client.post(self.url, data={'email': 'sometest.com'})
        assert resp.status_code == 400, 'Wrong email was provided, status not ok'
        assert resp.data['email'], 'Failing due to email an not something else'
        # Todo: test that no email was sent

    def test_registration_unknown_email(self, api_client):
        email = 'neverseenemail@test.com'
        resp = api_client.post(self.url, data={'email': email})
        # Todo: test for email sending
        assert resp.status_code == 201, 'Anonymous user sending correct email should work'

        assert UserToken.objects.get(email=email)

        # Test for resending a second time
        second_resp = api_client.post(self.url, data={'email': email})
        assert second_resp.status_code == 400, 'User should be notified if the has sent already'
        assert 'still valid' in second_resp.data['detail'], 'Message should contain that token is still valid'

        # Todo: directly get the token and register or use the session scope to create an entry

    def test_existing_email_address(self, api_client):
        user = get_user_model().objects.first()
        email = user.email
        resp = api_client.post(self.url, data={'email': email})
        assert resp.status_code == 400, 'can not register for existing user'


class TestRegistrationValidation:

    url = reverse_lazy('authorsi')

    def test_register_with_token(self, api_client):
        pass
