
import datetime as dt
# Create your views here.
from django.contrib.auth import get_user_model
from django.utils import timezone
from rest_framework import status
from rest_framework.generics import GenericAPIView
from rest_framework.permissions import AllowAny
from rest_framework.response import Response

from app import settings
from authorisation.helpers import generate_simple_token
from authorisation.models import UserToken
from users.serializers import RegistrationSerializer
from django.core.mail import send_mail, EmailMessage

from users.serializers import PasswordResetValidationSerializer, RegistrationValidationSerializer


class PasswordResetView(GenericAPIView):
    """
    User submits his email and a code is generated, saved to db and then sent via email
    Link: https://docs.djangoproject.com/en/2.1/_modules/django/contrib/auth/password_validation/
    """

    permission_classes = (AllowAny,)
    serializer_class = RegistrationSerializer

    def post(self, request):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        email = serializer.validated_data['email']
        # check if a profile exists
        try:
            user = get_user_model().objects.get(email=email)
            usr, created = UserToken.objects.get_or_create(email=user.email, type='password-reset')
            usr.token = generate_simple_token()
            usr.expires = timezone.now() + timezone.timedelta(days=settings.SIMPLE_TOKEN_EXPIRATION_DAYS)
            usr.save()

            email_args = dict(
                subject='Reset your Password for Motion',
                message='Hi customer,\ndidnt have time to make fancy email.\n' +
                        'Here is your confirmation code: ' + usr.token,
                from_email='motion@propulsion-academy.ch',
                recipient_list=[user.email],
                fail_silently=False,
            )
            send_mail(**email_args)

        except get_user_model().DoesNotExist:
            # Return successfull is somebody malicious tries to find out, which emails are in database
            # Then just do nothing.
            pass

        return Response(status=status.HTTP_201_CREATED, data={
            'detail': 'Token has been created and sent via email'})


class ValidateResetPasswordView(GenericAPIView):

    serializer_class = PasswordResetValidationSerializer
    permission_classes = (AllowAny,)

    def post(self, request):

        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        # All the validation is done in the serializer
        email = serializer.validated_data['email']
        token = serializer.validated_data['token']
        pw = serializer.validated_data['password']
        user = get_user_model().objects.get(email=email)
        user.set_password(pw)
        user.save()

        # Delete entry in UserToken
        entry = UserToken.objects.get(email=email, token=token)
        entry.delete()

        return Response(status=status.HTTP_201_CREATED, data={
            'detail': 'Password was successfully changed'
        })


class SendRegistrationTokenView(GenericAPIView):

    permission_classes = (AllowAny,)
    serializer_class = RegistrationSerializer

    def post(self, request):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        email = serializer.validated_data['email']

        # Todo: put that logic in the serializer

        try:
            get_user_model().objects.get(email=email)
            # Case Re-Registration
            email_args = dict(
                subject='Re-Registration on Motion',
                message='Hi customer,\n Seems like you or somebody else tries to register on motion the second time.',
                from_email='motion@propulsion-academy.ch',
                recipient_list=[email],
                fail_silently=False,
            )
            send_mail(**email_args)
            return Response(status=status.HTTP_400_BAD_REQUEST, data={
                'detail': 'This email can not be registered.'
            })
        except get_user_model().DoesNotExist:
            pass

        new_token_obj, created = UserToken.objects.get_or_create(email=email, type='registration')
        # new_token_obj.token = generate_simple_token()
        # new_token_obj.save()
        new_token_obj.generate_token()

        if created:
            # Case email was seen for the for first time, new registration
            new_mail = EmailMessage(
                subject='Validate your Registration for Motion',
                body='Hi customer,\ndidnt have time to make fancy email.\n' +
                        'Here is your confirmation code: ' + new_token_obj.token,
                # from_email='motion@propulsion-academy.ch',
                to=[email]
            )
            new_mail.send()
            return Response({'detail': 'This email can not be registered.'},
                            status=status.HTTP_201_CREATED)

        elif new_token_obj.expires > timezone.now():
            # Case user registers again too early before email was received.
            return Response(status=status.HTTP_400_BAD_REQUEST, data={
                'detail': 'Token sent by email ist still valid. No Email was sent.'
            })

        else:
            expiration = settings.SIMPLE_TOKEN_EXPIRATION_DAYS
            new_token_obj.token = generate_simple_token()
            new_token_obj.save()
            email_args = dict(
                subject='Validate your Registration for Motion',
                message='You did not register yet and your token has expired.\n' +
                        f'Give it another try (valid {expiration} days): ' + new_token_obj.token,
                from_email='motion@propulsion-academy.ch',
                recipient_list=[email],
                fail_silently=False,
            )
            send_mail(**email_args)
            return Response(status=status.HTTP_202_ACCEPTED, data={
                'detail': 'Email with registration code has been sent.'})


class RegistrationValidationView(GenericAPIView):

    permission_classes = (AllowAny,)
    serializer_class = RegistrationValidationSerializer

    def post(self, request):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()





