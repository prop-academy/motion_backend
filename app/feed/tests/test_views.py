import pytest
from django.conf import settings
from app.settings import MEDIA_ROOT
from django.contrib.auth import get_user_model
from django.core.files.uploadedfile import SimpleUploadedFile
from django.urls import reverse_lazy
from mixer.backend.django import mixer
from rest_framework import status

from blog.models import Post
from conftest import MOCK_POSTS_NUM
from friends.models import FriendRequest
from users.models import Profile, Like
import os
pytestmark = pytest.mark.django_db

class TestAllPostsView:

    url = reverse_lazy('feed:post-all')

    def test_successfull(self, user1_client):
        resp = user1_client.get(self.url)
        assert resp.status_code == 200
        assert len(resp.data) >= MOCK_POSTS_NUM, 'All users posts are more than just his own mocked'

    def test_anonymous(self, api_client):
        resp = api_client.get(self.url)
        assert resp.status_code == 403, 'View locked for anon'

    def test_search_post(self, user2, user1_client):
        new_post = mixer.blend(Post,
            title='test',
            content='Some zzzz in a sentence',
            owner=user2
        )
        url = self.url+'?search=zzzz'
        resp = user1_client.get(url)
        assert resp.status_code == 200
        assert resp.data[0]['content'] == 'Some zzzz in a sentence'


class TestPostsOfUser:

    @staticmethod
    def get_url(owner_id):
        return reverse_lazy('feed:posts-of-user', kwargs={'owner_id': owner_id})

    def test_successfull(self, user1_client, user2):
        resp = user1_client.get(self.get_url(user2.id))
        assert len(resp.data) == MOCK_POSTS_NUM

    def test_anonymous(self, api_client):
        resp = api_client.get(self.get_url(102))
        assert resp.status_code == 403, 'View locked for anon'

    def test_non_existing_user(self, user1_client):
        resp = user1_client.get(self.get_url(10000000))
        assert resp.status_code == 200
        assert len(resp.data) == 0, 'No posts returned for non existing user'


class TestPostsOfFollowees:

    url = reverse_lazy('feed:posts-of-followees')

    def test_successfull(self, user1_client, user1, user2):
        # user1 follows user2
        user1.profile.followees.add(user2)
        assert len(user1.profile.followees.all()) == 1, 'user1 should follow one user'
        resp = user1_client.get(self.url)
        assert len(resp.data) == MOCK_POSTS_NUM, 'should return the mocked posts of user 2'

    def test_anonymous(self, api_client):
        resp = api_client.get(self.url)
        assert resp.status_code == 403, 'View locked for anon'


class TestPostsOfFriends:
    url = reverse_lazy('feed:posts-of-friends')

    def test_successfull(self, user1_client, user2, user1, admin):

        mixer.blend(FriendRequest, sender=user1, receiver=user2, status=FriendRequest.ACCEPTED)
        mixer.blend(FriendRequest, sender=admin, receiver=user1, status=FriendRequest.ACCEPTED)

        resp = user1_client.get(self.url)
        assert resp.status_code == 200, 'Query should be successfull'
        assert len(resp.data) == 2 * MOCK_POSTS_NUM, 'Should return posts of admin and user2'

    def test_anonymous(self, api_client):
        resp = api_client.get(self.url)
        assert resp.status_code == 403, 'View locked for anon'



