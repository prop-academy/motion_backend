from django.shortcuts import render

# Create your views here.
from rest_framework.generics import ListAPIView

from blog.models import Post
from blog.serializers import PostSerializer
from friends.models import FriendRequest


class PostListApiView(ListAPIView):
    """
    Get a list of all the post of all users sorted from young to old
    """
    # Todo: add pagination
    serializer_class = PostSerializer

    def get_queryset(self):
        query = self.request.query_params.get('search', None)
        if query:
            return Post.objects.filter(content__contains=query)
        return Post.objects.all()


class PostUserListApiView(ListAPIView):
    """
    ists all the posts of a specific user in chronological order
    """
    serializer_class = PostSerializer

    def get_queryset(self):
        user_id = self.kwargs.get('owner_id')
        # Todo: include shared posts?
        return Post.objects.filter(owner_id=user_id).order_by('-created')


class FollowedPostListApiView(ListAPIView):
    """
    Get the list of all posts of the users that the logged in user is following
    """
    serializer_class = PostSerializer

    def get_queryset(self):
        user = self.request.user
        # posts = Post.objects.filter(owner__profile__followees=user)
        posts = Post.objects.filter(owner__followers__user=user)
        return posts


class FriendsPostListApiView(ListAPIView):
    """
    Get a list of posts that were published by your friends
    """
    serializer_class = PostSerializer

    def get_queryset(self):
        user = self.request.user
        # Todo: use the Q
        posts = Post.objects.filter(
            owner__received_requests__sender=user,
            owner__received_requests__status=FriendRequest.ACCEPTED) | \
            Post.objects.filter(
                owner__sent_requests__receiver=user,
                owner__sent_requests__status=FriendRequest.ACCEPTED)
        return posts
