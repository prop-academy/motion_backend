from django.urls import path

from feed.views import (PostListApiView, PostUserListApiView, FollowedPostListApiView,
                        FriendsPostListApiView)

app_name = 'feed'


urlpatterns = [
    path('feed/', PostListApiView.as_view(), name='post-all'),
    path('feed/<int:owner_id>/', PostUserListApiView.as_view(), name='posts-of-user'),
    path('feed/followees/', FollowedPostListApiView.as_view(), name='posts-of-followees'),
    path('feed/friends/', FriendsPostListApiView.as_view(), name='posts-of-friends')
]