from django.contrib import admin
from django.urls import path

from blog.views import PostDetailView, PostCreateView, LikedPostsView, LikePostView, SharePostApiView

app_name = 'blog'

urlpatterns = [
    path('posts/<int:post_id>/', PostDetailView.as_view(), name='post-detail'),
    path('posts/new-post/', PostCreateView.as_view(), name='post-new'),
    path('posts/likes/', LikedPostsView.as_view(), name='posts-liked'),
    path('posts/like/<int:post_id>/', LikePostView.as_view(), name='post-like'),
    path('posts/share-post/<int:post_id>/', SharePostApiView.as_view(), name='post-share')
]