
import pytest
from app.settings import MEDIA_ROOT
from django.core.files.uploadedfile import SimpleUploadedFile
from django.urls import reverse_lazy
from mixer.backend.django import mixer

from blog.models import Post
from users.models import Profile, Like
import os
pytestmark = pytest.mark.django_db

default_pic = os.path.join(MEDIA_ROOT, Profile.image.__dict__['field'].default)

new_image = SimpleUploadedFile(
    name='test_image.jpg',
    content=open(default_pic, 'rb').read(),
    content_type='image/jpeg')


class TestPostDetail:

    @staticmethod
    def get_url(post_id):
        return reverse_lazy('blog:post-detail', kwargs={'post_id': post_id})


    def test_get_a_post(self, user1_client, user2_client, user1, user2):

        post1 = Post.objects.filter(owner=user1).first()

        # Test getting own post
        resp = user1_client.get(self.get_url(post1.id))
        assert resp.status_code == 200, 'User should be able to retrieve own post'

        # Test getting post from other user
        resp = user2_client.get(self.get_url(post1.id))
        assert resp.status_code == 200, 'User2 should be able to retrieve post of user1'

    def get_non_existing_post(self, user1_client):
        resp = user1_client.get(self.get_url(1000000000000))
        assert resp.status_code == 400

    def test_patch_a_post(self, user1_client, user2_client, user1_post):
        data = {
            'title': 'new title'
        }
        user1_client.patch(self.get_url(user1_post.id), data=data)
        user1_post.refresh_from_db()
        assert user1_post.title == 'new title'

        # test un allowed patch
        resp = user2_client.patch(self.get_url(user1_post.id))
        assert resp.status_code == 403, 'Forbidden for other user to modify post'


class TestCreateNewPost:

    url = reverse_lazy('blog:post-new')

    def test_successfull(self, user2_client, user2):
        data = {
            'owner': user2.id,
            'title': 'T',
            'content': 'C',
            'image': new_image
        }
        resp = user2_client.post(self.url, data)
        assert resp.status_code == 201, 'New post with picture should be created'

        post = Post.objects.filter(owner_id=user2.id).last()
        assert post.content == 'C', 'The new post should be the last in the database'

    def test_anonymous(self, api_client):
        resp = api_client.post(self.url)
        assert resp.status_code == 403, 'Forbidden to use this endoint for anon'


class TestLikedPostsList:

    url = reverse_lazy('blog:posts-liked')

    def test_get_list(self, user1_client, user1, user2_post):
        mixer.blend(Like, post_id=user2_post.id, user_id=user1.id)
        resp = user1_client.get(self.url)
        assert len(resp.data) == 1, 'Should return one liked post'

    def test_anonymous(self, api_client):
        resp = api_client.get(self.url)
        assert resp.status_code == 403, 'Forbidden for anon'


class TestLikeAPost:

    @staticmethod
    def get_url(post_id):
        return reverse_lazy('blog:post-like', kwargs={'post_id': post_id})

    def test_successful(self, user1_post, user2_client, user2):
        resp = user2_client.post(self.get_url(user1_post.id))
        assert resp.status_code == 201, 'Liking post of other use should work'

        created_like = Like.objects.filter(user=user2, post=user1_post)
        assert created_like.exists(), 'The new created like should be in the database'

    def test_like_own_post(self, user1, user1_client, user1_post):
        resp = user1_client.post(self.get_url(user1_post.id))
        assert resp.status_code == 400, 'Liking own post should not work'


    def test_like_non_existing_post(self, user1_client):
        resp = user1_client.post(self.get_url(10000000000))
        assert resp.status_code == 404, 'Should return not found'
        assert 'Not found' in resp.data['detail']

    def test_anonymous(self, api_client):
        resp = api_client.post(self.get_url(10000000000))
        assert resp.status_code == 403, 'Should be forbidden for anon'


class TestShareAPost:

    @staticmethod
    def get_url(post_id):
        return reverse_lazy('blog:post-share', kwargs={'post_id': post_id})

    def test_successfull(self, user1_client, user2_post, user1):
        resp = user1_client.post(self.get_url(user2_post.id))
        assert resp.status_code == 201, 'should be able to share the post'
        try:
            new_shared = Post.objects.get(shared_id = user2_post.id, owner_id=user1.id)
        except Post.DoesNotExist:
            assert False, 'There should be a new shared post'

    def test_share_own_post(self, user1, user1_post, user1_client):
        resp = user1_client.post(self.get_url(user1_post.id))
        assert resp.status_code == 400, 'Should not be able to like own post'
