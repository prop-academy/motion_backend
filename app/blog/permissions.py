from rest_framework.permissions import BasePermission


class IsOwnerOfObjectorGet(BasePermission):

    def has_object_permission(self, request, view, obj):

        # allow get, head and options
        m = request.method
        if m == 'GET' or m == 'OPTIONS' or m == 'HEAD':
            return True

        if obj.owner == request.user: return True
        return False


class IsUserOfObject(BasePermission):

    def has_object_permission(self, request, view, obj):
        if obj.user == request.user: return True
        return False
