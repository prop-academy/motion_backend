from django.conf import settings
from django.db import models


# Create your models here.


class Post(models.Model):
    title = models.CharField(verbose_name='title', max_length=100, null=True)

    image = models.ImageField(blank=True, null=True, upload_to='post-pics')

    content = models.TextField(max_length=3000, null=True)
    created = models.DateField(verbose_name='posted', auto_now_add=True)
    owner = models.ForeignKey(
        to=settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        related_name='posts',
        blank=True, null=True
    )

    shared = models.ForeignKey(
        to='self',
        on_delete=models.CASCADE,
        blank=True,
        null=True
    )

    def __str__(self):
        if self.title and self.owner:
            return self.title + " by " + self.owner.username
        elif self.shared:
            return 'Shared: ' + str(self.shared_id)
