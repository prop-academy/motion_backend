from http.client import HTTPResponse

# Create your views here.
from rest_framework import status
from rest_framework.generics import ListAPIView, RetrieveUpdateDestroyAPIView, CreateAPIView, get_object_or_404, \
    GenericAPIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from blog.models import Post
from blog.permissions import IsOwnerOfObjectorGet
from blog.serializers import PostSerializer
from users.models import Like


class PostDetailView(RetrieveUpdateDestroyAPIView):
    queryset = Post.objects.all()
    lookup_url_kwarg = 'post_id'
    serializer_class = PostSerializer
    # Todo: isAdmin default usable here of create IsOwnerOrAdmin?
    permission_classes = (IsAuthenticated, IsOwnerOfObjectorGet)


class PostCreateView(CreateAPIView):
    serializer_class = PostSerializer

    def perform_create(self, serializer):
        assert not self.request.user.is_anonymous, "This view should be login protected!"
        serializer.save(owner=self.request.user)


class LikedPostsView(ListAPIView):
    serializer_class = PostSerializer

    def get_queryset(self):
        user = self.request.user
        return Post.objects.filter(likes__user=user)


class LikePostView(GenericAPIView):
    lookup_url_kwarg = 'post_id'
    queryset = Post.objects.all()

    def post(self, request, post_id):
        current_post = self.get_object()
        # No permission class here,
        if current_post.owner == request.user: return Response(
            status=status.HTTP_400_BAD_REQUEST, data={'detail': 'Can not like your own post'})

        like, created = Like.objects.get_or_create(post=current_post, user=request.user)
        if created:
            return Response(status=status.HTTP_201_CREATED, data={'detail': 'ok'})
        return Response(status=status.HTTP_400_BAD_REQUEST, data={'detail': 'already liked'})

    def delete(self, request, post_id):
        current_post = self.get_object()
        record = get_object_or_404(Like, post=current_post, user=request.user)
        record.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class SharePostApiView(GenericAPIView):
    """
    Share a post of another user and add also a title or additional content.
    """
    lookup_url_kwarg = 'post_id'
    queryset = Post.objects.all()
    serializer_class = PostSerializer

    # todo evemtually add pssoibility t serialze title and content additionally

    def post(self, request, post_id):
        post_to_like = self.get_object()
        if post_to_like.owner == request.user:
            return Response(status=status.HTTP_400_BAD_REQUEST, data={
                'detail': 'User can not share hwis own post'
            })
        shared_post, created = Post.objects.get_or_create(owner=self.request.user, shared=post_to_like)
        seri = PostSerializer(instance=shared_post)
        if created:
            return Response(status=status.HTTP_201_CREATED, data=seri.data)
        return Response(status=status.HTTP_400_BAD_REQUEST, data={'detail': 'Post already shared'})
