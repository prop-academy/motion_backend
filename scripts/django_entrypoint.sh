#!/bin/bash

# Add local user
# Either use the LOCAL_USER_ID if passed in at runtime or
# fallback

USER_ID=${LOCAL_USER_ID:-9001}

echo "Starting with UID : $USER_ID"
#useradd --shell /bin/bash -u $USER_ID -o -c "" -m user
#export HOME=/home/user
echo "usermod user to $USER_ID"
usermod -u $USER_ID user

echo "changing ownership of static and media directory with UID ${USER_ID}"

chown -R $USER_ID:$USER_ID /static-files
chown -R $USER_ID:$USER_ID /media-files
chown -R $USER_ID:$USER_ID /app

echo "Performing checks and listing directories:"
echo
echo "Getting UID of user (default 1000): $(id -u user)"

ls -la | grep files
ls -la | grep app

exec gosu user "$@"