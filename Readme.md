# Doc

## Starting the app for the first time

```bash

# Build and tag it with imgae name in specified in d-c file
docker-compose build app

# Db and app start-up, run management commands
docker-compose up -d # runs sshd on app from d-c file
# Execute one-time commands inside already running container
docker-compose exec app python manage.py migrate
docker-compose exec app python manage.py createsuperuser
```
On errors:
Inside the container, make sure shat `(app) root@....:/app#` is 
on your terminal, meaning the app environment of conda is activated
when you run `docker-compose run app bash`.

Run management commands:
```bash
python manage.py migrate
python manage.py createsuperuser
```

## Using the debugger

Since the python environment is now inside a running container and 
not on our machine, the Pycharm debugger has to connect to the 
executing python process started up for our script, so it has to be
able to somehow connect to the container.  
The Dockerfile in here has sshd installed, and the development
docker-compose file starts sshd deamon (deamon = background service).  
When docker-compose has been run, app will run sshd.

Pycharm configuration for remote ssh interpreter:  
![pycharm-ssh-interpreter-is-nowhere](./assets/pics)


## Docker file

- using gosu entrypoint for file mountings in developer mode

## JWT

- server access tokens will be checked 
- and if not valid again, get a new authentification token

## Queries

- Get location of the current logged user:
    - `request.user.profile.location`
- return all the users that live in Zürich:
    - `User.objects.filter(user_profile__location='Zürich')`
- give back all posts that were liked by laurent (username):
    - `Post.objects.filter(likes__user__username='laurent')`
- give back all followers of laurent
    - `Ùser.objects.filter(followers__following__username='laurent')`, permonant
    - `Ùser.objects.get(username="Laurent").followers` then make list comprehension to get user 
    instead of user_profiles
- All users that have been posted yesterday ba user big L.
    ```
    today = datetime.utcnow().date()
    yesterday = today - datetime.timedelta(day=1)
    Post.objects.filter(user__username="big L", created__gte=yesterday, create__lte=today)
    ```
- Get all the all posts created by user colin that contain the word flat earth
    `Post.objects.filter(user__username="colin", likes__is_null=False, content__contains="flat earth")`
   
- Find all the users that liked laurents posts:
    `Ùser.objects.filter(likes__post__user__username="laurent")`
    
- Get all the posts of the poeple that have been liked by laurent's followees
    `Posts.objects.filter(likes__user__user_profile_followees__username="laurent")`
    
- get all the user profiles that have no likes but one post the id 1:
    - `UserProfile.objects.filter(user__likes__post__id=1)`
    
- Get all the user profiles that have no likes but one or more posts.
    - `UserProfile.objects.filter(followees__is_null=True, user__posts__is_null=False)`
    
- Sent Laurent a friend request
    - get user lauren`receiver = User.objects.get(username='laurent')`
    - create FriedRequest instance `FriendRequest(sender=request.user, reveiver=receiver)`
  
## JWT Token authentification  

Get the first token that is valid as long as noted in `JWT_REFRESH_EXPIRATION_DELTA`.
For every api request, you send this token token in the HTTP authorisation header.

- `api-token-auth`: Get a token with a post request with username and password.
- `api-token-refresh`: 
- `api-token-verify`

What is a bearer token?
  

## Extract Data from the database

### Migrations

To go back to a previous state, you can specify where to migrate:

`python manage.py migrate appname 0001_initial`

If the revert was successfull, delete all migrations files younger than the one.
Change back your code to the state you want.

### Drop Database

- drop the schema public (database can have mor then one schema)
    `drop schema public cascade;`
    `create schema public;`
- delete all migrations
- and then makemigratrions and 

### Dump Load data

Make a folder (proposal) with `mkdir fixtures` on the top level of the folder app.

- `python manage.py dumpdata > database.json`
- `python manage.py loaddata fixtures/database.json`

What is the best way to drop the database when you want to renex it or restore?

### Nested serializers

Nested Serializers are handy for GET requests, for example when
the UserSerializer is called and you want to fill in the profile as well,
that is handled by the ProfileSerializer.

### Restrict CRUD to the owner of objects

You can use a permission classes in a file `permissions.py` on an
app level to make sure, the user can just change or see his own 
objects.  

`DEFAULT_PERMISION_CLASSES` get overwritten when `permission_classes` is specified
in an view.

### Custom Permission classes

Use for logic that is used in different views that does checks in the database.

### Improvements

- pip install django-extensions: use TimeStampedModel


### Important notes

#### pytest.ini

When `addopts --cov --cov-report` is activated, the debugger is not working for tests.