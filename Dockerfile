
FROM continuumio/miniconda:4.4.10

ENV LANG=C.UTF-8 LC_ALL=C.UTF-8
ENV NOTVISIBLE "in users profile"
ENV PYTHONUNBUFFERED 1

RUN groupadd --gid 1000 user \
&& useradd --uid 1000 --gid user --shell /bin/bash --create-home user

RUN apt-get update && apt-get upgrade -y && apt-get install -qqy \
    wget \
    bzip2 \
    libssl-dev \
    openssh-server \
    gosu; \
    rm -rf /var/lib/apt/lists/*; \
    gosu nobody true

RUN mkdir /var/run/sshd
RUN echo 'user:screencast' | chpasswd
#RUN echo 'root:screencast' | chpasswd
#RUN sed -i '/PermitRootLogin/c\PermitRootLogin yes' /etc/ssh/sshd_config

# SSH login fix. Otherwise user is kicked off after login
RUN sed 's@session\s*required\s*pam_loginuid.so@session optional pam_loginuid.so@g' -i /etc/pam.d/sshd
RUN echo "export VISIBLE=now" >> /etc/profile

RUN mkdir -p /app && \
    mkdir -p /media-files && \
    mkdir -p /static-files

COPY ./app/requirements.yml /app/requirements.yml
RUN /opt/conda/bin/conda env create -f /app/requirements.yml

ENV PATH /opt/conda/envs/app/bin:$PATH
#RUN sed '$ a conda activate app' -i /root/.bashrc
RUN echo ". /opt/conda/etc/profile.d/conda.sh" >> /home/user/.bashrc && \
    echo "conda activate app" >> /home/user/.bashrc

COPY ./app /app

RUN chown -R user:user /app && \
    chown -R user:user /media-files && \
    chown -R user:user /static-files

COPY ./scripts/* /scripts/
RUN chmod +x /scripts/*

WORKDIR /app

EXPOSE 8000
EXPOSE 22